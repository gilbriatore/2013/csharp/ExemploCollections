﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ExemploCollections
{
    class Program
    {
        static void Main(string[] args)
        {

            //Vetor de números
            int[] vetor = new int[5];
            vetor[0] = 8;
            vetor[1] = 9;

            //Lista de números
            ArrayList listaDeNumeros = new ArrayList();
            listaDeNumeros.Add(8);
            listaDeNumeros.Add(9);            

            //Vetor de strings
            string[] strings = new string[3];
            strings[0] = "Texto 1";
            strings[1] = "Texto 2";

            //Lista de string
            ArrayList listaDeStrings = new ArrayList();
            listaDeStrings.Add("Texto 1");
            listaDeStrings.Add("Texto 2");

            for (int i = 0; i < listaDeStrings.Count; i++)
			{
			    object o = listaDeStrings[i];
			}

            foreach (object item in listaDeStrings)
            {
                Console.WriteLine(item.ToString());
            }


            //Vetor de pessoas
            Pessoa[] vetorDePessoas = new Pessoa[3];

            //Lista de pessoas
            ArrayList listaDePessoas = new ArrayList();
            listaDePessoas.Add(new Pessoa());
            listaDePessoas.Add(new Pessoa());
            listaDePessoas.Add(new Pessoa());
            //listaDePessoas.Add(new Navio());

            foreach (object obj in listaDePessoas)
            {
                if (obj is Pessoa){
                   Pessoa p = (Pessoa) obj;
                }

                if (obj is Navio)
                {
                    Navio n = (Navio)obj;
                }

            }


            //Lista genérica de pessoas
            List<Pessoa> lista = new List<Pessoa>();
            lista.Add(new Pessoa());
            //lista.Add(new Navio());


            Hashtable map = new Hashtable();
            map.Add("1234546", new Pessoa());
            map.Add("3265474", new Pessoa());
            map.Add("2315447", new Pessoa());

            if (map.Contains("1234546"))
            {
                //faz alguma coisa..
            }

            //Parada
            Console.ReadLine();

        }
    }


    class Pessoa
    {
        string nome;
        string idade;
    }
}
